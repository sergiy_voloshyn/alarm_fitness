package com.xyz.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.concurrent.BrokenBarrierException;

/**
 * Created by user on 22.02.2018.
 */
/*
https://stackoverflow.com/questions/1082437/android-how-to-use-alarmmanager
 */
public class AlarmReceiver extends BroadcastReceiver{
    public final String ALARM_ACTION="com.xyz.alarm.AlarmReceiver";


    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context," ALARM",Toast.LENGTH_LONG).show();

    }
}
